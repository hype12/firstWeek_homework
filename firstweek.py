#encoding='utf8'
'''
（本周共计2个必做作业，1个扩展作业）
 1. 使用文件和目录操作，定义一个统计指定目录大小的函数（注意目录中还有子目录）。 
 2. 完成本周1.15的阶段案例《飞机大战》游戏中没有完成的部分。 
	 ①. 完成敌机发射子弹功能（注意：子弹不是连发、移动速度不要太快） 
	 ②. 实现敌机子弹和玩家飞机的碰撞检测
	 ③. 为消失的飞机添加爆炸效果 
 3. (扩展题) 自定义设计和开发一款游戏：如贪吃蛇、拼图、坦克大战等
'''

import time
import fjdz
#文件夹复制及计算文件夹大小

def copyFile(filepath1='./aaa',filepath2='./aaa1'):
	'''文件复制：filepath1 代表源文件；filepath2 代表目标文件'''
	try:
		#打开两个文件
		file1=open(filepath1,'r+',encoding='utf8')
		file2=open(filepath2,'w+',encoding='utf8')
		#读取源文件内容，写入目标文件中
		file2.writelines(file1.readlines())
		#关闭文件
		file1.close()
		file2.close()
		print('文件复制完成')
	except FileNotFoundError:
		print('源文件找不到,无法复制！')

def copyDir(dir1='./aaa',dir2='./bbb'):
	'''文件夹复制（多层级目录及文件）'''
	# 得到原文件夹的文件列表
	import os
	try:
		filelist=os.listdir(dir1)
		#创建目标文件夹
		if os.path.exists(dir2):
			print('目标文件夹已经存在,不再新建！')
			pass
		else:
		   #重新创建文件夹
			os.mkdir(dir2)
		for f in filelist:
			file1=os.path.join(dir1,f)
			file2=os.path.join(dir2,f)
			#判断是文件还是文件夹
			if os.path.isfile(file1):
				print('文件:',file1)
				copyFile(file1,file2)
			if os.path.isdir(file1):
			#是文件夹：递归调用copyDir函数
				print('文件夹:',file1)
				copyDir(file1,file2)
	except FileNotFoundError as info:
		print('错误提示：',info)


def getDirSize(dir1='./aaa'):
	'''获取文件夹文件大小'''
	filesize=0
	# 得到文件夹的文件列表
	import os
	try:
		filelist=os.listdir(dir1)
		# 遍历文件夹中文件列表
		for f in filelist:
			file1=os.path.join(dir1,f)
			#判断是文件还是文件夹
			if os.path.isfile(file1):
 				filesize+=os.path.getsize(file1)
 				print(os.path.abspath(file1),'大小为：',os.path.getsize(file1))
			if os.path.isdir(file1):
			#是文件夹：递归调用getDirSize函数
				filesize+=getDirSize(file1)
		print(os.path.abspath(dir1),'大小为：',filesize)
		return filesize #返回文件夹总大小
	except FileNotFoundError as info:
		print('错误提示：',info)


def init_game_fjdz():
	'''调用初始化游戏函数'''
	fjdz.main()
	

def init_game_tcs():
	'''调用初始化游戏函数'''
	print('调用初始化贪吃蛇游戏函数')	

def go_back():
	'''函数：倒计时返回主函数入口'''
	for a in range(3,0,-1):
	 	print('倒计时3s...退出:{:2}s'.format(a))
	 	time.sleep(1)
	init_work()


#作业检查入口
def init_work():
	#定义菜单字典
	menulist=[
	{'xh':1,'name':'复制文件夹','description':'复制文件夹会将本目录下的aaa文件夹复制出aaa1文件夹','fun':'copyDir'},
	{'xh':2,'name':'计算文件夹大小','description':'默认计算aaa文件夹中文件总大小','fun':'getDirSize'},
	{'xh':3,'name':'飞机大战小游戏','description':'运行飞机大战小游戏','fun':'init_game_fjdz'},
	{'xh':4,'name':'退出','description':'退出','fun':'quit'}
	]
	#分割线
	print("="*12,"按以上菜单选择","="*14)

	#生成菜单目录
	for i in menulist:
		print('{}:{}'.format(i['xh'],i['name']))

	#分割线
	print("="*12,"按以上菜单选择","="*14)

	#获取输入的值，并判断输入的值
	key = input("请输入对应的数字：")
	while True:
		#判断按键是否为有效输入
	 	if int(key)>len(menulist):
	 		print('按键无效，请重新输入！')
	 		go_back()
	 	#输出所选的菜单内容
	 	print("您选择的是：",menulist[int(key)-1]['name'])
	 	time.sleep(1)
	 	print("具体描述：",menulist[int(key)-1]['description'])
	 	#输入判断
	 	instr=input('是否确定运行(Y/N)?:')
	 	if instr.upper()=='Y':
	 		#调用对应菜单的入口函数：
	 		eval(menulist[int(key)-1]['fun'])()

	 		go_back()
	 	else:
	 		go_back()

#运行作业内容
init_work()