#encoding='utf8'
#飞机大战游戏代码（跟着视频学习写的）
import pygame
from pygame.locals import *
import time,random

gameName='飞机大战游戏'
bgimage='./images/bg2.jpg'

def key_control(wanjia):
	'''键盘控制函数'''
	#程序退出
	for event in  pygame.event.get():
		if event.type == QUIT:
			print("exit()")
			exit()
	#获取键盘按键信息
	pressed=pygame.key.get_pressed()
	#判断按键，执行对象相关的操作
	if pressed[K_RIGHT]:
		print('按下了右键')
		wanjia.mv_right()
	elif pressed[K_LEFT]:
		print('按下了左键')
		wanjia.mv_left()
	elif pressed[K_UP]:
		print('按下了上键')
		wanjia.mv_up()
	elif pressed[K_DOWN]:
		print('按下了下键')
		wanjia.mv_down()
	if pressed[K_SPACE]:
		print('按下了空格键')
		wanjia.fire()
	if pressed[K_TAB]:
		print('按下了TAB键')

class Hero:
	'''游戏玩家类'''
	def __init__(self,screen):
		self.x=200
		self.y=400
		self.screen=screen
		self.image=pygame.image.load('./images/me.png')
		self.bullet_list=[] #玩家子弹列表
		self.hit_em_list=[] #记录玩家打中的敌人的列表
		self.nohit_em_list=[] #记录未打中敌人的列表
	def display(self):

		for b in self.bullet_list:
			b.display()
			if b.move(1):
				self.bullet_list.remove(b)

		'''绘制玩家'''
		self.screen.blit(self.image,(self.x,self.y))
		
		'''四个方向移动飞机方法'''
	def mv_left(self):
		self.x-=5
		if self.x <= 0 :
			self.x=0
	def mv_right(self):
		if self.x >= 406:
			self.x= 406

		self.x+=5
	def mv_up(self):
		self.y-=5
		if self.y <=0:
			self.y=0

	def mv_down(self):
		self.y+=5
		if self.y>=470:
			self.y=470
	def fire(self):
		'''子弹添加'''
		self.bullet_list.append(Bullet(self.screen,self.x+53,self.y))

class Bullet:
	'''子弹类'''
	def __init__(self,screen,x,y):
		self.x=x
		self.y=y
		self.screen=screen
		self.image=pygame.image.load('./images/pd.png')

	def display(self):
		self.screen.blit(self.image,(self.x,self.y))

	def move(self,flag):
		if flag==1:#代表玩家发射的子弹
			self.y-=10
			if self.y <= -20:
				return True
		else:
			self.y+=5
			if self.y>=600:
				return True


	#添加子弹击中爆炸效果
	def hit(self):
		hitimage=['./images/bomb0.png','./images/bomb1.png','./images/bomb2.png','./images/bomb3.png']
		for i in hitimage:
			self.screen.blit(pygame.image.load(i),(self.x-53,self.y-20))

class EnemyP:
	'''敌人机器'''
	def __init__(self,screen):
		self.x=random.choice(range(0,409))
		self.y=-80
		self.screen=screen
		self.image=pygame.image.load('./images/e2.png')
		self.bullet_list=[] #敌人子弹列表

	def display(self):
		for b in self.bullet_list:
			b.display()
			if b.move(0):
				self.bullet_list.remove(b)

		self.screen.blit(self.image,(self.x,self.y))

	def move(self,wanjia):
		self.y+=4
		if self.y >= 650:
			return True,False
		#遍历子弹，执行碰撞检测
		for bu in wanjia.bullet_list:
			if bu.x>self.x+12 and bu.x<self.x+92 and bu.y>self.y+20 and bu.y<self.y+60:
				bu.hit()  #调用子弹击中敌人的效果
				wanjia.bullet_list.remove(bu) 
				return True,True
		#设置，当敌人和玩家在同一直线上时。让敌人发射子弹
		if wanjia.x==self.x:
			print('添加了子弹')
			self.bullet_list.append(Bullet(self.screen,self.x+53,self.y))
		return False,False


def main():
	'''主程序函数'''

	m=-958
	emlist=[] #存放敌人列表
	#创建游戏窗口
	screen=pygame.display.set_mode((512,550),0,0)
	#设置窗口的标题
	pygame.display.set_caption(gameName)
	

	#创建一个游戏的背景
	bg=pygame.image.load(bgimage)

	#创建玩家
	wanjia=Hero(screen)

	while not pygame.key.get_pressed()[K_TAB]:
		#设置游戏首页
		pygame.font.init()
		myfont=pygame.font.Font('C:/Windows/Fonts/simhei.ttf',14)
		textImage=myfont.render("飞机大战游戏",True,(255,255,255))
		textImage1=myfont.render("按tab键进入游戏",True,(255,255,255))
		screen.blit(textImage,(200,100))
		screen.blit(textImage1,(200,200))

		#绘制玩家飞机
		wanjia.display()
		#执行键盘事件调用
		key_control(wanjia)
		#更新显示
		pygame.display.update()

		#定时显示
		time.sleep(0.05)

	while True:
		m+=2
		if m>=-190:
			m=-958
	    #绘制画面
		screen.blit(bg,(0,m))


 		#绘制玩家飞机
		wanjia.display()

		#执行键盘事件调用
		key_control(wanjia)
		#显示已经击中的敌人数量
		screen.blit(pygame.font.Font('C:/Windows/Fonts/simhei.ttf',12).render('打中敌人数量：'+str(len(wanjia.hit_em_list)),True,(255,255,255)),(380,20))
		#显示错过的敌人数量
		screen.blit(pygame.font.Font('C:/Windows/Fonts/simhei.ttf',12).render('未打中敌人数量：'+str(len(wanjia.nohit_em_list)),True,(255,255,255)),(40,20))
		
		#随机绘制敌人
		if random.choice(range(50)) in (1,20):
			emlist.append(EnemyP(screen))

		#遍历敌人，并绘制、移动
		for em in emlist:
			em.display()
			#获取敌人移动的两种状态：移动出界面和被击中
			result=em.move(wanjia)
			# print(result)
			if result[0]:
				wanjia.nohit_em_list.append(em) #记录未打中的敌人
				emlist.remove(em)
			if result[1]:
				wanjia.hit_em_list.append(em) #记录被打中的敌人

		#更新显示
		pygame.display.update()

		#定时显示
		time.sleep(0.04)



#判断是否是主函数运行
if __name__=='__main__':
	main()
